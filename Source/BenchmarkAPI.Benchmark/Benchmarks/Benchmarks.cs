﻿using System.Threading.Tasks;
using BenchmarkAPI.Benchmark.GraphQl;
using BenchmarkAPI.Benchmark.Grpc;
using BenchmarkAPI.Benchmark.HotChocolate;
using BenchmarkAPI.Benchmark.RestApi;
using BenchmarkDotNet.Attributes;

namespace BenchmarkAPI.Benchmark.Benchmarks
{
    [ArtifactsPath("D:/BenchmarkResult")]
    [MemoryDiagnoser]
    [RPlotExporter]
    [MinColumn, MaxColumn, AllStatisticsColumn]
    public class Benchmarks
    {
        [Params(100)]
        public int IterationCount;

        private readonly RestApiClientService _restClientService = new RestApiClientService();
        private readonly GrpcClientService _grpcClientService = new GrpcClientService();
        private readonly GraphQlClientService _graphQlClientService = new GraphQlClientService();
        private readonly HotChocolateClientService _hotChocolateClientService = new HotChocolateClientService();

        #region GetApiInfo

        [Benchmark]
        public async Task RestApi_GetApiInfoAsync()
        {
            for (var i = 0; i < IterationCount; i++)
            {
                await _restClientService.GetApiInfoAsync();
            }
        }

        [Benchmark]
        public async Task Grpc_GetApiInfoAsync()
        {
            for (var i = 0; i < IterationCount; i++)
            {
                await _grpcClientService.GetApiInfoAsync();
            }
        }

        [Benchmark]
        public async Task GraphQl_GetApiInfoAsync()
        {
            for (var i = 0; i < IterationCount; i++)
            {
                await _graphQlClientService.GetApiInfoAsync();
            }
        }

        [Benchmark]
        public async Task HotChocolate_GetApiInfoAsync()
        {
            for (var i = 0; i < IterationCount; i++)
            {
                await _hotChocolateClientService.GetApiInfoAsync();
            }
        }

        #endregion

        #region GetAllProducts

        [Benchmark]
        public async Task RestApi_GetAllProductsAsync()
        {
            for (var i = 0; i < IterationCount; i++)
            {
                await _restClientService.GetAllProductsAsync();
            }
        }

        [Benchmark]
        public async Task Grpc_GetAllProductsAsync()
        {
            for (var i = 0; i < IterationCount; i++)
            {
                await _grpcClientService.GetAllProductsAsync();
            }
        }

        [Benchmark]
        public async Task GraphQl_GetAllProductsAsync()
        {
            for (var i = 0; i < IterationCount; i++)
            {
                await _graphQlClientService.GetAllProductsAsync();
            }
        }

        [Benchmark]
        public async Task HotChocolate_GetAllProductsAsync()
        {
            for (var i = 0; i < IterationCount; i++)
            {
                await _hotChocolateClientService.GetAllProductsAsync();
            }
        }

        #endregion

        #region

        [Benchmark]
        public async Task RestApi_AddProductAsync()
        {
            for (var i = 0; i < IterationCount; i++)
            {
                await _restClientService.GetAllProductsAsync();
            }
        }

        [Benchmark]
        public async Task Grpc_AddProductAsync()
        {
            for (var i = 0; i < IterationCount; i++)
            {
                await _grpcClientService.GetAllProductsAsync();
            }
        }

        [Benchmark]
        public async Task HotChocolate_AddProductAsync()
        {
            for (var i = 0; i < IterationCount; i++)
            {
                await _hotChocolateClientService.GetAllProductsAsync();
            }
        }

        #endregion
    }
}
