﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BenchmarkAPI.GrpcService;
using BenchmarkAPI.Shared.Models;
using GraphQL;
using GraphQL.Client.Abstractions;
using GraphQL.Client.Http;
using GraphQL.Client.Serializer.Newtonsoft;
using Newtonsoft.Json;

namespace BenchmarkAPI.Benchmark.GraphQl
{
    public class GraphQlClientService
    {
        private readonly IGraphQLClient _client;

        public GraphQlClientService()
        {
            _client = new GraphQLHttpClient("http://localhost:8003/graphql", new NewtonsoftJsonSerializer());
        }

        public async Task<string> GetApiInfoAsync()
        {
            var query = new GraphQLRequest
            {
                Query = @"
                {
                  api_info{
                    message
                  }
                }"
            };
            var response = await _client.SendQueryAsync<object>(query, CancellationToken.None);

            var apiInfoType = JsonConvert.DeserializeObject<ApiInfoType>(response.Data.ToString() ?? string.Empty);

            return apiInfoType.ApiInfo.Message;
        }

        public async Task<List<ProductReply>> GetAllProductsAsync()
        {
            var query = new GraphQLRequest
            {
                Query = @"
                {
                  products{
                    id,
                    categoryId,
                    title,
                    description,
                    price,
                    rating
                  }
                }"
            };
            var response = await _client.SendQueryAsync<object>(query);
            var responseProductCollectionType = JsonConvert.DeserializeObject<ResponseProductCollectionType>(response.Data.ToString() ?? string.Empty);

            return responseProductCollectionType.Products;
        }

        public class ResponseProductCollectionType
        {
            public List<ProductReply> Products { get; set; }
        }

        public class ApiInfoType
        {
            [JsonProperty(PropertyName = "api_info")]
            public ApiInfo ApiInfo { get; set; }
        }
    }
}
