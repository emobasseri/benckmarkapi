﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BenchmarkAPI.GrpcService;
using Grpc.Core;

namespace BenchmarkAPI.Benchmark.Grpc
{
    public class GrpcClientService
    {
        private readonly ProductProto.ProductProtoClient _client;

        public GrpcClientService()
        {
            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);

            var channel = new Channel("localhost:8002", ChannelCredentials.Insecure);
            _client = new ProductProto.ProductProtoClient(channel);
        }

        public async Task<string> GetApiInfoAsync()
        {
            var response = await _client.GetApiInfoAsync(new EmptyRequest());

            return response.Message;
        }

        public async Task<List<ProductReply>> GetAllProductsAsync()
        {
            var products = new List<ProductReply>();

            var response = _client.GetAllProducts(new EmptyRequest()).ResponseStream;

            while (await response.MoveNext())
            {
                products.Add(response.Current);
            }

            return products;
        }

        public async Task<ProductReply> AddProduct()
        {
            var product = new AddProductRequest
            {
                CategoryId = 1,
                Title = "Test Product",
                Description = "Product Description",
                Filename = "p.jpg",
                Price = 125.7,
                Rating = 5
            };

            var addedProduct = await _client.AddProductAsync(product);
            return addedProduct;
        }
    }
}
