﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using BenchmarkAPI.GrpcService;
using GraphQL;
using GraphQL.Client.Abstractions;
using GraphQL.Client.Http;
using GraphQL.Client.Serializer.Newtonsoft;
using Newtonsoft.Json;

namespace BenchmarkAPI.Benchmark.HotChocolate
{
    public class HotChocolateClientService
    {
        private readonly IGraphQLClient _client;

        public HotChocolateClientService()
        {
            _client = new GraphQLHttpClient("http://localhost:8004/graphql", new NewtonsoftJsonSerializer());
        }

        public async Task<string> GetApiInfoAsync()
        {
            var query = new GraphQLRequest
            {
                Query = @"
                {
                  apiInfo{
                    message
                  }
                }"
            };
            var response = await _client.SendQueryAsync<object>(query, CancellationToken.None);

            var apiInfoType = JsonConvert.DeserializeObject<ApiInfoType1>(response.Data.ToString() ?? string.Empty);

            return apiInfoType.ApiInfo.Message;
        }

        public async Task<List<ProductReply>> GetAllProductsAsync()
        {
            var query = new GraphQLRequest
            {
                Query = @"
                {
                  allProducts{
                    id,
                    categoryId,
                    title,
                    description,
                    price,
                    rating
                  }
                }"
            };
            var response = await _client.SendQueryAsync<object>(query);
            var responseProductCollectionType = JsonConvert.DeserializeObject<ResponseProductCollectionType>(response.Data.ToString() ?? string.Empty);

            return responseProductCollectionType.Products;
        }

        public async Task<ProductReply> AddProductAsync()
        {
            var query = new GraphQLRequest
            {
                Query = @"
                    mutation{
                    addProduct(input: {
                        categoryId : 1,
                        title : ""Test Product"",
                        description: ""Product Description"",
                        filename: ""p.jpg"",
                        price : 123.5,
                        rating : 5
                     }){
                        id,
                        categoryId,
                        title,
                        description,
                        price,
                        rating
                      }}"
            };
            var response = await _client.SendMutationAsync<object>(query);
            var responseProductType = JsonConvert.DeserializeObject<ResponseProductType>(response.Data.ToString() ?? string.Empty);

            return responseProductType.Product;
        }

        public class ResponseProductCollectionType
        {
            public List<ProductReply> Products { get; set; }
        }

        public class ResponseProductType
        {
            [JsonProperty(PropertyName = "addProduct")]
            public ProductReply Product { get; set; }
        }

        public class ApiInfoType1
        {
            public ApiInfo ApiInfo { get; set; }
        }
    }
}
