﻿using System;
using System.Threading.Tasks;
using BenchmarkDotNet.Running;

namespace BenchmarkAPI.Benchmark
{
    class Program
    {
        static async Task Main(string[] args)
        {
            BenchmarkRunner.Run<Benchmarks.Benchmarks>();
            Console.ReadKey();
        }
    }
}
