﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BenchmarkAPI.GrpcService;
using Newtonsoft.Json;

namespace BenchmarkAPI.Benchmark.RestApi
{
    public class RestApiClientService
    {
        private readonly HttpClient _client;

        public RestApiClientService()
        {
            _client = GetHttpClient();
        }

        private HttpClient GetHttpClient()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return client;
        }

        public async Task<string> GetApiInfoAsync()
        {
            var response = await _client.GetStringAsync("http://localhost:8001/api/products/GetApiInfo");

            var apiInfo = JsonConvert.DeserializeObject<ApiInfo>(response);

            return apiInfo.Message;
        }


        public async Task<List<ProductReply>> GetAllProductsAsync()
        {
            var response = await _client.GetStringAsync("http://localhost:8001/api/products");
            
            return JsonConvert.DeserializeObject<List<ProductReply>>(response);
        }

        public async Task<ProductReply> AddProductAsync()
        {
            var product = new AddProductRequest
            {
                CategoryId = 1,
                Title = "Test Product",
                Description = "Product Description",
                Filename = "p.jpg",
                Price = 125.7,
                Rating = 5
            };

            var content = new StringContent(JsonConvert.SerializeObject(product),Encoding.UTF8, "application/json");
            var response = await _client.PostAsync("http://localhost:8001/api/products", content);
            var responseString = await response.Content.ReadAsStringAsync(CancellationToken.None);

            return JsonConvert.DeserializeObject<ProductReply>(responseString);
        }
    }
}
