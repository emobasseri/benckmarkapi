﻿using System.Collections.Generic;
using System.Linq;
using BenchmarkAPI.GraphQLAPI.GraphQL.Types;
using BenchmarkAPI.GrpcService;
using BenchmarkAPI.Shared.Mappings;
using BenchmarkAPI.Shared.Models;
using BenchmarkAPI.Shared.Services;
using GraphQL;
using GraphQL.Types;

namespace BenchmarkAPI.GraphQLAPI.GraphQL.Queries
{
    public class ProductQuery : ObjectGraphType
    {
        private readonly IProductService _service;
        public ProductQuery()
        {
            _service = new ProductService();

            Field<ApiInfoType>("api_info", 
                resolve: context => GetApiInfo());

            Field<ListGraphType<ProductType>>("products",
                arguments: GetProductQueryArguments(),
                resolve: context => GetAllProducts());

            Field<ProductType>("product", 
                arguments: GetProductQueryArguments(),
                resolve: context =>
                {
                    var id = context.GetArgument<int>("id");

                    return GetAllProducts().FirstOrDefault(e => e.Id == id);
                });

        }

        private ApiInfo GetApiInfo()
        {
            return new ApiInfo {Message = "API Version 1.0"};
        }

        private QueryArguments GetProductQueryArguments()
        {
            var queryQrguments = new QueryArguments();
            queryQrguments.Add(new QueryArgument<IdGraphType> { Name = "id", Description = "The Id of the Product." });
            return queryQrguments;
        }
        private List<ProductReply> GetAllProducts()
        {
            var products = _service.GetAllProducts();
            return ProductMapper.MapProducts(products);
        }
    }
}
