﻿using System;
using BenchmarkAPI.GraphQLAPI.GraphQL.Queries;
using Microsoft.Extensions.DependencyInjection;

namespace BenchmarkAPI.GraphQLAPI.GraphQL.Schema
{
    public class AppSchema : global::GraphQL.Types.Schema
    {
        public AppSchema(IServiceProvider provider) : base(provider)
        {
            Query = provider.GetRequiredService<ProductQuery>();
        }
    }
}
