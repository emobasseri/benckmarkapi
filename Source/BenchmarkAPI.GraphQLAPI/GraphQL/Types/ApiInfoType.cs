﻿using BenchmarkAPI.GrpcService;
using BenchmarkAPI.Shared.Models;
using GraphQL.Types;

namespace BenchmarkAPI.GraphQLAPI.GraphQL.Types
{
    public class ApiInfoType : ObjectGraphType<ApiInfo>
    {
        public ApiInfoType()
        {
            Field(e => e.Message).Description("Message property of the ApiInfo");
        }
    }
}
