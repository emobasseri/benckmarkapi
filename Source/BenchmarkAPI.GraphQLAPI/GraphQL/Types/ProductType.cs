﻿using BenchmarkAPI.GrpcService;
using GraphQL.Types;

namespace BenchmarkAPI.GraphQLAPI.GraphQL.Types
{
    public sealed class ProductType : ObjectGraphType<ProductReply>
    {
        public ProductType()
        {
            Field(e => e.Id, type: typeof(IdGraphType)).Description("Id property of the Product entity");
            Field(e => e.CategoryId).Description("CategoryId property of the Product entity");
            Field(e => e.Title).Description("Title property of the Product entity");
            Field(e => e.Description).Description("Description property of the Product entity");
            Field(e => e.Price).Description("Price property of the Product entity");
            Field(e => e.Rating).Description("Rating property of the Product entity");
        }
    }
}
