using System.Threading.Tasks;
using BenchmarkAPI.Shared.Mappings;
using BenchmarkAPI.Shared.Services;
using Grpc.Core;

namespace BenchmarkAPI.GrpcService.Services
{
    public class ProductService : ProductProto.ProductProtoBase
    {
        private readonly IProductService _productService;
        public ProductService()
        {
            _productService = new Shared.Services.ProductService();
        }

        public override Task<ApiInfo> GetApiInfo(EmptyRequest request, ServerCallContext context)
        {
            return Task.FromResult(new ApiInfo
            {
                Message = "API Version 1.0"
            });
        }

        public override async Task GetAllProducts(EmptyRequest request, IServerStreamWriter<ProductReply> responseStream, ServerCallContext context)
        {
            var products = _productService.GetAllProducts();

            foreach (var product in products)
            {
                var productReply = ProductMapper.MapProduct(product);
                await responseStream.WriteAsync(productReply);
            }
        }

        public override Task<ProductReply> AddProduct(AddProductRequest request, ServerCallContext context)
        {
            var product = ProductMapper.MapProductRequest(request);

            _productService.AddProduct(product);

            var productReply = ProductMapper.MapProduct(product);

            return Task.FromResult(productReply);
        }
    }
}
