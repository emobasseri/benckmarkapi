﻿using System.Threading.Tasks;
using BenchmarkAPI.GrpcService;
using BenchmarkAPI.Shared.Mappings;
using BenchmarkAPI.Shared.Services;

namespace BenchmarkAPI.HotChocoloteAPI.Queries
{
    public class ProductMutations
    {
        private readonly IProductService _productService;
        public ProductMutations()
        {
            _productService = new ProductService();
        }

        public async Task<ProductReply> AddProductAsync(AddProductRequest input)
        {
            var product = ProductMapper.MapProductRequest(input);

            _productService.AddProduct(product);

            var productReply = ProductMapper.MapProduct(product);

            return await Task.FromResult(productReply);
        }
    }
}
