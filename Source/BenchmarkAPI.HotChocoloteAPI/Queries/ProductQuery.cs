﻿using System.Collections.Generic;
using BenchmarkAPI.GrpcService;
using BenchmarkAPI.Shared.Mappings;
using BenchmarkAPI.Shared.Models;
using BenchmarkAPI.Shared.Services;

namespace BenchmarkAPI.HotChocoloteAPI.Queries
{
    public class ProductQuery
    {
        private readonly IProductService _service;

        public ProductQuery()
        {
            _service = new ProductService();
        }

        public ApiInfo GetApiInfo()
        {
            var apiInfo = new ApiInfo
            {
                Message = "API Version 1.0"
            };

            return apiInfo;
        }

        public List<ProductReply> GetAllProducts()
        {
            var products = _service.GetAllProducts();

            return ProductMapper.MapProducts(products);
        }
    }
}
