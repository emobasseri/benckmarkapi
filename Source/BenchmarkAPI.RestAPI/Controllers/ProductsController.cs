﻿using System.Threading;
using System.Threading.Tasks;
using BenchmarkAPI.GrpcService;
using BenchmarkAPI.Shared.Mappings;
using BenchmarkAPI.Shared.Models;
using BenchmarkAPI.Shared.Services;
using Microsoft.AspNetCore.Mvc;

namespace BenchmarkAPI.RestAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly IProductService _productService;
        public ProductsController()
        {
            _productService = new ProductService();
        }

        [HttpGet("GetApiInfo")]
        public IActionResult GetApiInfo()
        {
            var apiInfo = new ApiInfo
            {
                Message = "API Version 1.0"
            };

            return Ok(apiInfo);
        }

        [HttpGet]
        public IActionResult GetAllAsync()
        {
            var products = _productService.GetAllProducts();
            var result = ProductMapper.MapProducts(products);

            return Ok(result);
        }

        [HttpPost]
        public IActionResult AddProduct(AddProductRequest productRequest, CancellationToken cancellationToken = default)
        {
            var product = ProductMapper.MapProductRequest(productRequest);

            _productService.AddProduct(product);

            return Ok(product);
        }
    }
}
