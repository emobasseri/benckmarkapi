﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.Json.Serialization;
using BenchmarkAPI.Shared.Models;
using Newtonsoft.Json;

namespace BenchmarkAPI.Shared.Data
{

    public class ApplicationContext
    {
        public List<Product> Products { get; set; }
        public List<Category> Categories { get; set; }

        public ApplicationContext()
        {
            ReadData();
        }

        private void ReadData()
        {
            //if (Categories == null)
            //{
            //    var assembly = Assembly.GetExecutingAssembly();
            //    var resourceStream = assembly.GetManifestResourceStream("BenchmarkAPI.Shared.Data.categories.json");

            //    using var reader = new StreamReader(resourceStream, Encoding.UTF8);
            //    Categories = JsonConvert.DeserializeObject<List<Category>>(reader.ReadToEnd());
            //}

            if (Products == null)
            {
                var assembly = Assembly.GetExecutingAssembly();
                var resourceStream = assembly.GetManifestResourceStream("BenchmarkAPI.Shared.Data.products.json");

                using var reader = new StreamReader(resourceStream, Encoding.UTF8);
                Products = JsonConvert.DeserializeObject<List<Product>>(reader.ReadToEnd());
            }
        }
    }
}
