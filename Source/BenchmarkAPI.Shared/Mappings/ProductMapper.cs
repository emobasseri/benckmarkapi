﻿using System.Collections.Generic;
using System.Linq;
using BenchmarkAPI.GrpcService;
using BenchmarkAPI.Shared.Models;

namespace BenchmarkAPI.Shared.Mappings
{
    public class ProductMapper
    {
        public static List<ProductReply> MapProducts(List<Product> products)
        {
            return products.Select(e => MapProduct(e)).ToList();
        }
        public static ProductReply MapProduct(Product product)
        {
            if (product == null)
                return null;

            return new ProductReply
            {
                Id = product.Id,
                CategoryId = product.CategoryId,
                Title = product.Title,
                Description = product.Description,
                Filename = product.Filename,
                Price = product.Price,
                Rating = product.Rating
            };
        }

        public static Product MapProductRequest(AddProductRequest product)
        {
            return new Product
            {
                CategoryId = product.CategoryId,
                Title = product.Title,
                Description = product.Description,
                Filename = product.Filename,
                Price = product.Price,
                Rating = product.Rating
            };
        }
    }
}
