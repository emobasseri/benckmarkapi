﻿namespace BenchmarkAPI.Shared.Models
{
    public class Product
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Filename { get; set; }
        public double Price { get; set; }
        public int Rating { get; set; }
    }
}