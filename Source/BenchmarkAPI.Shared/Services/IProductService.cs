﻿using System.Collections.Generic;
using BenchmarkAPI.Shared.Models;

namespace BenchmarkAPI.Shared.Services
{
    public interface IProductService
    {
        List<Product> GetAllProducts();
        Product GetProductById(int id);
        Product AddProduct(Product product);
    }
}