﻿using System.Collections.Generic;
using System.Linq;
using BenchmarkAPI.Shared.Data;
using BenchmarkAPI.Shared.Models;

namespace BenchmarkAPI.Shared.Services
{
    public class ProductService : IProductService
    {
        private readonly ApplicationContext _context;

        public ProductService()
        {
            _context = new ApplicationContext();
        }
        public List<Product> GetAllProducts()
        {
            return _context.Products;
        }

        public Product GetProductById(int id)
        {
            return _context.Products.FirstOrDefault(e => e.Id == id);
        }

        public Product AddProduct(Product product)
        {
            var maxId = _context.Products.Max(e => e.Id);
            product.Id = maxId + 1;

            _context.Products.Add(product);

            return product;
        }
    }
}
